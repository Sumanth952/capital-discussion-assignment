provider "aws" {
  region = "us-east-1"
  assume_role {
      role_arn = "arn:aws:iam::85775798442:role/terraformiac"
    }
}

terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.6.0"
    }
  }

  required_version = "~> 1.0"
}

