

module "eks_k8s1" {
    source  = "terraform-aws-modules/eks/aws"
    version = "2.3.1"
  
    cluster_version = var.cluster_version
  
    cluster_name = var.cluster_name
    Vpc_id = var.Vpc_id
  
    subnets = ["subnet-00000001", "subnet-000000002", "subnet-000000003"]
  
    cluster_endpoint_private_access = "true"
    cluster_endpoint_public_access  = "true"
  
    write_kubeconfig      = true
    config_output_path    = "/.kube/"
    manage_aws_auth       = true
    write_aws_auth_config = true
  
    map_users = [
      {
        user_arn = "arn:aws:iam::85775798442:user/sumanth9523"
        username = "sumanth9523"
      },
    ]
  
    worker_groups = [
      {
        name                 = "workers"
        instance_type        = "t2.large"
        asg_min_size         = 1
        asg_desired_capacity = 1
        asg_max_size         = 3
        root_volume_size     = 100
        root_volume_type     = "gp2"
        ami_id               = "ami-03be126fdbb92daa1"
        ebs_optimized     = false
        key_name          = "all"
        
      },
    ]
  
    tags = {
      Cluster = var.cluster_name
    }
  }

  




