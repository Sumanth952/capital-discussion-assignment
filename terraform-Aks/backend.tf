terraform {
  backend "s3" {
    bucket = var.bucket
    prefix = "capital/dev-service-project"
    key    = var.key
    region = "us-east-1"
  }
}